#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

void initSudoku();
bool sudoku(uint8_t board[81]);

void printBoard(uint8_t board[81]) {
    for (uint8_t i = 0; i < 81; i++) {
        printf("%u ", board[i]);
        if ((i+1) % 9 == 0) {
            printf("\n");
        }
    }
    printf("\n");
}

int main() {
    uint8_t testsum1[81] = {
        0, 0, 9, 7, 4, 8, 0, 0, 0,
        7, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 1, 0, 9, 0, 0, 0,
        0, 0, 7, 0, 0, 0, 2, 4, 0,
        0, 6, 4, 0, 1, 0, 5, 9, 0,
        0, 9, 8, 0, 0, 0, 3, 0, 0,
        0, 0, 0, 8, 0, 3, 0, 2, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 6,
        0, 0, 0, 2, 7, 5, 9, 0, 0
    };

    uint8_t testsum2[81] = {
        0, 0, 6, 5, 0, 0, 0, 0, 4,
        0, 0, 0, 8, 9, 0, 0, 0, 0,
        0, 0, 5, 6, 0, 1, 9, 3, 0,
        5, 1, 0, 0, 0, 0, 8, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 9, 1,
        0, 3, 8, 2, 0, 5, 1, 0, 0,
        0, 0, 0, 0, 3, 6, 0, 0, 0,
        6, 0, 0, 0, 0, 9, 7, 0, 0
    };

    uint8_t testsum3[81] = {
        5, 3, 0, 0, 7, 0, 0, 0, 0, 
        6, 0, 0, 1, 9, 5, 0, 0, 0, 
        0, 9, 8, 0, 0, 0, 0, 6, 0, 
        8, 0, 0, 0, 6, 0, 0, 0, 3, 
        4, 0, 0, 8, 0, 3, 0, 0, 1, 
        7, 0, 0, 0, 2, 0, 0, 0, 6, 
        0, 6, 0, 0, 0, 0, 2, 8, 0, 
        0, 0, 0, 4, 1, 9, 0, 0, 5, 
        0, 0, 0, 0, 8, 0, 0, 7, 9
    };


    initSudoku();

    if (sudoku(testsum1)) {
        printBoard(testsum1);
    }

    if (sudoku(testsum2)) {
        printBoard(testsum2);
    }
    
    if (sudoku(testsum3)) {
        printBoard(testsum3);
    }

    return 0;
}
