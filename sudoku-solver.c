#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

// inspired from http://bit.ly/1zfIGMc

// typedef uint8_t[81] boardNums;

// enable or diable logs
#define log_printf(fmt, ...) (0)
//#define log_printf printf

struct maskState {
    uint16_t row[9], col[9], blk[9];
};

static const uint16_t ALL_POSS = 0b1111111110;
static uint8_t ROW[81], COL[81], BLK[81];

struct maskState getmask(uint8_t board[81]) {
    struct maskState ms;

    for (int i = 0; i < 9; i++) {
        ms.row[i] = ms.col[i] = ms.blk[i] = ALL_POSS;
    }

    for (uint16_t i = 0; i < 81; i++) {
        if (board[i] != 0) {
            uint16_t mask;

            mask = ALL_POSS ^ (1 << board[i]);

            ms.row[ROW[i]] &= mask;
            ms.col[COL[i]] &= mask;
            ms.blk[BLK[i]] &= mask;
        }
    }

    return ms;
}

bool sudoku(uint8_t board[81]) {
    struct maskState ms;
    bool filled;
    uint8_t try_poss, try_idx;
    uint16_t try_mask;
    uint8_t try_board[81];

    ms = getmask(board);

    do {
        filled = false;
        try_poss = 10;

        for (uint8_t i = 0; i < 81; i++) {
            uint16_t mask, poss; 
            uint8_t poss_cnt;

            if (board[i] != 0) { // already filled
                continue;
            }

            poss = ms.row[ROW[i]] & ms.col[COL[i]] & ms.blk[BLK[i]];
            poss_cnt = __builtin_popcount(poss);
            if (poss_cnt == 1) { // only one possibility
                board[i] = __builtin_ctz(poss);
                log_printf("setting %u,%u to %u\n", ROW[i], COL[i], board[i]);
                filled = true;

                mask = ~(1 << board[i]);

                ms.row[ROW[i]] &= mask;
                ms.col[COL[i]] &= mask;
                ms.blk[BLK[i]] &= mask;
            } else if (poss_cnt == 0) {
                log_printf("failed at %d\n", i);
                return false;
            } else if ((!filled) && (try_poss > poss_cnt)) { // find minimum path constraint
                try_idx = i;
                try_poss = poss_cnt;
                log_printf("later try %u with %u poss\n", i, poss_cnt);
            }
        }
    } while (filled);

    if (try_poss == 10) { // never tried to set try_idx
        return true;
    }

    log_printf("need to try %u with %u poss\n", try_idx, try_poss);

    try_mask = ms.row[ROW[try_idx]] & ms.col[COL[try_idx]] & ms.blk[BLK[try_idx]];
    for (uint8_t i = 1 ; i < 10 ; i++) {
        if (try_mask & (1 << i)) { // i is possible to be set at board[try_idx]
            memcpy(try_board, board, 81*sizeof(uint8_t));
            log_printf("trying %u at %u\n", i, try_idx);
            try_board[try_idx] = i;
            if (sudoku(try_board)) {
                memcpy(board, try_board, 81*sizeof(uint8_t));
                return true;
            }
        }
    }

    return false;
}

void initSudoku() {
    static bool inited = false;

    if (inited) {
        return;
    }
    for (uint16_t i = 0; i < 81; i++) {
        ROW[i] = i / 9;
        COL[i] = i % 9;
        BLK[i] = ((ROW[i] / 3) * 3) + (COL[i] / 3);
    }
    inited = true;
}

void solveSudoku(char** board, int boardRowSize, int boardColSize) {
    uint8_t bn[81];
    char numchar;

    assert(boardRowSize == 9);
    assert(boardColSize == 9);
    initSudoku();

    for(uint16_t i = 0; i < 81; i++) {
        numchar = board[ROW[i]][COL[i]];
        bn[i] = (numchar == '.') ? 0 : (numchar - '0');
    }

    if (!sudoku(bn)) {
        printf("can't solve\n");
    }

    for(uint16_t i = 0; i < 81; i++) {
        board[ROW[i]][COL[i]] = '0' + bn[i];
    }
}
